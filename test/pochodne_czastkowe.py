#Oblicz jezeli istnieja pochodne czastkowe df/dx(0,0) oraz df/dy(0,0)

import numpy as np
import matplotlib.pyplot as plt

def pochodna1(a):
    a=a.replace('x', '0')
    return a.replace('x', '0')

def eval_with_variable(a):
    return 0

def freplace(a,how):
    a=a.replace('x',how[0])
    return a.replace('y',how[1])

temp='\sqrt[3]{x^3-y^3}'
text=temp

plt.rcParams['text.usetex'] = True

fig, ax = plt.subplots(figsize=(7, 3.5))
text='f(x,y)='+'$'+text+'$'
ax.set_title(text, fontsize=16, color='r')
fd0=freplace(text,'d0')
fd0=fd0[fd0.find('$'):]
fd0=fd0.replace('$','')
f00=freplace(text,'00')
f00=f00[f00.find('$'):]
f00=f00.replace('$','')
a='{' + fd0 + ' - ' + f00 + '}'
b='{d}'
r='$\lim_{\delta x\\to 0} ' + r'\frac{}{}$'.format(a,b)
print(r)

ax.text(0.1, 0.8, r'$\frac{\delta f}{\delta x}$ = $\lim_{\delta x\to 0} \frac{f(\delta x, 0)-f(0, 0)}{\delta x}$ = ' + r, bbox=dict(facecolor='red', alpha=0.5))
plt.show()