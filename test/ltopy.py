import re
import sympy as sym

def mono_lsqrt(a):
     s = (a.find('\sqrt'))
     e = (a[:s+5]).find('}')
     exp = a[s+5:e] + '}'
     pows = exp.find('[')
     powe = exp.find(']')
     if pows != -1:
        power = (exp[pows + 1:powe])
     else:
        power = '2'
     texps=exp.find('{')
     texpe=exp.find('}')
     expresion = '((' + (exp[texps+1:texpe]) + ') ** (1/' + power + '))'
     return expresion

def lsqrt(a):
    ind = ([m.start() for m in re.finditer('sqrt', a)])
    l=[]
    for i in ind:
        t = a[i-1:].find('}')
        b=(a[i-1:])
        l.append(b[:t]+'}')
    for i in l:
        a=a.replace(i,mono_lsqrt(i))
    return a

def lpower(a):
    return a.replace('^','**')

def llim(a):
    s = (a.find(r'\lim'))
    if s == -1:
        return a
    b = (a[s:])
    c = (b[:b.find('}')+1])
    #print(a.replace(c,'A'))
    d=(c[c.find('{')+1:-1])
    e=d[d.find(r'\to')+3:]
    f=d[:d.find(r'\to')]
    return(('sym.limit(' + a.replace(c,'') + ',' + f + ',' + e + ')'))

def mono_lfrac(i,a):
    b=(a[i-1:])
    s=''
    temp=0
    for i in b:
        s+=i
        if i == '}':
            temp+=1
        if temp == 2:
            break
    g=s.replace(r'\frac','')
    right = (g[g.find('}{')+2:-1])
    left = (g[1:g.find('}{')])
    return a.replace(s,'((' + left + ')/(' + right + '))')

def lfrac(a):
    ind = ([m.start() for m in re.finditer('frac', a)])
    ind.sort(reverse=True)
    for i in ind:
        a = mono_lfrac(i, a)
    return a

def lmtosympy(a):
    a=lsqrt(a)
    a=lfrac(a)
    a=llim(a)
    #a=test(a)
    a=lpower(a)
    return a

temp=r'\lim_{d\to 0} \frac{\frac{d^3+0}{d^2+0^2} - \frac{0^3+0}{0^2+0^2}}{d}'
text=temp
text=lmtosympy(text)
d = sym.Symbol('d')
d = sym.Symbol('d')
str = '((((d**3-0**3) ** (1/3)) - ((0**3-0**3) ** (1/3))) / (d))'
