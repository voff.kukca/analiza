import sympy as sym


def integratexy(func, odx, dox, ody, doy):
    x = sym.Symbol('x')
    y = sym.Symbol('y')
    return sym.integrate(sym.integrate(func, (x, odx, dox)), (y, ody, doy))

def integrateyx(func, ody, doy, odx, dox):
    x = sym.Symbol('x')
    y = sym.Symbol('y')
    return sym.integrate(sym.integrate(func, (y, ody, doy)), (x, odx, dox))

def integratepr(func, odr, dor, odp, dop):
    r = sym.Symbol('r')
    p = sym.Symbol('p')
    return sym.integrate(sym.integrate(func, (r, odr, dor)), (p, odp, dop))