import sympy as sym
from test import doubint
from test.doubint import integratepr


def zamiana(func, xd, yd):
    if xd != 0:
        zxd = '(r * cos(p)' + '+' + str(xd) + ')'
    else:
        zxd = '(r * cos(p))'
    if yd != 0:
        zyd = '(r * sin(p)' + '+' + str(yd) + ')'
    else:
        zyd = '(r * sin(p))'
    return '('+str(func.replace('y',zyd).replace('x',zxd)) + ')*r'

#func = input('wpisz funkcje')
func = 'x**2 + y**2'
#print('a', integratepr('(r * sym.cos(p))**2 + (r * sym.sin(p)+3)**2', '0', '2', '-sym.pi/2', 'sym.pi/2'))
#r = sym.Symbol('r')
#p = sym.Symbol('p')
#print(sym.integrate('(r * cos(p))**2 + (r * sin(p)+3)**2', (r, 0, 2)))
odr = 0
dor = 0
odp = 0
dop = 2*sym.pi
xd = 0
yd = 0
print(zamiana(func,xd,yd))
print('podstawa to abstraktne kolo o srodku 0,0, wpisz promien')
dor=input()
r = input('aby modyfikowac kolo wpisz tak')
if r == 'tak':
    while(r != 'y'):
        print('1. aby podzielic kolo na dwie czesci w plaszyznie x')
        print('2. aby podzielic kolo na dwie czesci w plaszyznie y')
        print('3. aby przxemeszczyc kolo w plaszczyznie x')
        print('4. aby przxemeszczyc kolo w plaszczyznie y')
        print('5. mniejsze kolo w srodku')
        r = int(input())
        if r == 1:
            odp = -sym.pi/2
            dop = sym.pi/2
            print('hmmm')
        elif r == 2:
            odp = 0
            dop = sym.pi
        elif r == 3:
            xd = input('na ile')
        elif r == 4:
            yd = input('na ile')
        elif r == 5:
            odr=input('promien')
        else:
            "liczby 1-5"
        print('odr = ', odr, ', dor = ', dor, ', odp = ',odp, ', dop = ', dop, 'yd' , yd, 'xd' , xd)
        r = input('koniec?y,n')

nfunc = zamiana(func, xd, yd)
print('nowy granicy calkowania , to:', odr, ', dor = ', dor, ', odp = ',odp, ', dop = ', dop, 'yd' , yd, 'xd' , xd)
print('nowa funkcja: ' , nfunc)
print('a', integratepr(nfunc, odr, dor, odp, dop))