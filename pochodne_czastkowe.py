import matplotlib.pyplot as plt
import re
import sympy as sym
from test.ltopy import lmtosympy
import math

def podst_func(func):
    return text.replace('x', '0').replace('y', '0'),text.replace('x', 'd').replace('y', '0'),text.replace('x', '0').replace('y', 'd')

def poch_czstk(text):
    f00,fd0,f0d = podst_func(text)
    t=((lmtosympy(f00)[1:-1]))
    if t.find(')/(') != -1:
        if((eval(t[:t.find(')/(')+1])) == 0 and (eval(t[t.find(')/(')+2:])) == 0):
            f00 = '0'
    lim1=(r'\lim_{d\to 0} \frac{' + fd0 + ' - ' + f00 + '}{d}')
    lim2=(r'\lim_{d\to 0} \frac{' + f0d + ' - ' + f00 + '}{d}')
    text1 = lmtosympy(lim1)
    text2 = lmtosympy(lim2)
    d = sym.Symbol('d')
    ans1=(eval(text1))
    ans2=(eval(text2))
    return ans1,ans2,lim1,lim2

text = input('f(x,y) in latex format')
ans1,ans2,lim1,lim2 = poch_czstk(text)
print('df/dx = ' , ans1)
print('df/dy = ' , ans2)
