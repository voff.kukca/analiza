import sympy as sym

x = sym.Symbol('x')
y = sym.Symbol('y')

func=input('f(x,y)')
p=input('[]<=x<=[],[]<=y<=[]')
odx = (p[:p.find('<=x')])
dox = (p[p.find('x<=')+3:p.find(',')])
ody = (p[p.find(',')+1:p.find('<=y')])
doy = (p[p.find('y<=')+3:])
intx = (sym.integrate(func, (x, odx, dox)))
inty = (sym.integrate(intx, (y, ody, doy)))
print(inty)
#0<=x<=2,0<=y<=1
#x**2/(y**2+1)

#1<=x<=2,3<=y<=4
#1/((x+y)**3)

#0<=x<=1,0<=y<=1
#log(x+y+1)