import sympy as sym

def readable_for_sympy(func):
    t = []
    for i in func:
        t.append(str(i[4:])+' -y')
    return t

def punkty(func):
    p = []
    func = readable_for_sympy(func)
    for i in range(len(func)-1):
        for j in range(i+1, len(func)):
            x = sym.Symbol('x')
            y = sym.Symbol('y')
            # print(func[i])
            print(sym.solve((func[i], func[j]), (x, y)))
    return p

temp = 'y = x, y = x + 3, y = -2 * x, y = - 2 * x + 6'
#func = input().split(', ')
func = temp.split(', ')
print(punkty(func))