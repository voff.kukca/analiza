import sympy as sym
import math

from sympy import true


def pds(func,dane):
    temp=''
    #print(func,dane)
    for i in range(len(str(func))):
        if str(func)[i] == 'x' and str(func)[i-1] != 'e' and str(func)[i+1] !='p':
            temp+=str(dane[0])
        else:
            temp+=str(func)[i]
    #print(sym.simplify(temp.replace('y',str(dane[1]))))
    return sym.simplify(temp.replace('y',str(dane[1])))

func = input('f(x,y)')
x = sym.Symbol('x')
y = sym.Symbol('y')
dx=(sym.diff(func, x))
print(dx)
dy=(sym.diff(func, y))
print(dy)
ans = (sym.solve((dx, dy), (x, y)))
print(ans)
tans = []
for i in ans:
    if(str(i) == 'x'):
        temp=[]
        temp.append(ans[x])
        temp.append(ans[y])
        tans.append(temp)
        break
    else:
        ile=0
        for j in i:
            if str(j).find('I')!=-1:
                ile+=1
        if ile == 0:
            tans.append(i)
print(tans)
dxx=(sym.diff(dx, x))
#print(dxx)
dxy=(sym.diff(dx, y))
#print(dxy)
dyx=(sym.diff(dy, x))
#print(dyx)
dyy=(sym.diff(dy, y))
#print(dyy)
#print(pds(dxx,i))
for i in tans:
    if(pds(dxx,i)*pds(dyy,i)-pds(dxy,i)*pds(dyx,i))>0:
        print('w '+str(i)+' istnieje ekstremum')
        if(pds(dxx,i))>0:
            print('minimum')
        else:
            print('maksimum')
    else:
        print('w ' + str(i) + ' nie istnieje ekstremum')